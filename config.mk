PRODUCT_SOONG_NAMESPACES += \
    vendor/pixelstar-prebuilts/common

# Inherit from telephony config
$(call inherit-product, vendor/pixelstar-prebuilts/products/telephony.mk)

# Inherit from gms config
$(call inherit-product, vendor/pixelstar-prebuilts/products/gms.mk)

# Inherit from audio config
$(call inherit-product, vendor/pixelstar-prebuilts/audio.mk)

# Inherit from rro_overlays config
$(call inherit-product, vendor/pixelstar-prebuilts/rro_overlays.mk)
